<?php

/**
 * @file
 * Clever page callbacks.
 */

/**
 * Handles incoming requests from Clever.
 */
function clever_redirect_page() {
  global $user;

  $access_token = '';
  $params = drupal_get_query_parameters();

  $code = clever_get_query_parameter('code', $params);
  $scopes = clever_get_query_parameter('scope', $params);
  $state = clever_get_query_parameter('state', $params, FALSE);

  if (!$code || !$scopes) {
    $error = t('An error occurred with your request from Clever. Please try the login link again.');
    return clever_render_error(CLEVER_ERROR_BAD_PARAMETER, $error);
  }

  if ($user->uid) {
    // A user is already logged in.
    $access_token = clever_get_oauth_access_token_from_session($user);
  }

  // 1. Request an OAuth2 access token from Clever.
  if (!$access_token) {
    // Access token is not available. Exchange code for access token.
    $access_token = clever_get_oauth_tokens($code, $state);

    if (!$access_token) {
      $error = t('An error occurred with your request from Clever. Please try the login link again.');
      return clever_render_error(CLEVER_ERROR_NO_TOKEN, $error);
    }

    $_SESSION['CLEVER_ACCESS_TOKEN'] = $access_token;
  }

  // 2. Authorize the user.
  $me = clever_request('me', 'GET', $access_token);
  if (empty($me)) {
    $error_key = CLEVER_ERROR_BAD_TOKEN;
    $error = t('An error occurred with your credentials. Please try the login link again.');
    $_SESSION['CLEVER_ACCESS_TOKEN'] = '';

    if ($user->uid) {
      $error_key = CLEVER_ERROR_BAD_TOKEN_AUTHED;
      $error = t('An error occurred with your credentials. Please log out of my.zaner-bloser.com, and log in with Clever again.');
    }

    return clever_render_error($error_key, $error);
  }

  // 3. Confirm the user exists.
  $user_data = clever_user_load_me($me, $access_token);
  $account = $user_data['account'];
  $account_data = $user_data['data'];

  if (!$account) {
    // User account does not exist.
    $error = t('A user account matching your information does not exist. Please contact your School Administrator or the Site Administrator.');
    return clever_render_error(CLEVER_ERROR_NO_ACCOUNT, $error);
  }
  elseif ($user->uid > 0 && $account->uid !== $user->uid) {
    // User account does not match the logged in user.
    $error = t('You are already logged in as a different user. Please log out and try again.');
    return clever_render_error(CLEVER_ERROR_WRONG_USER, $error);
  }
  elseif (!user_access('use clever sso', $account)) {
    // User account does not have any role that allows for Clever SSO usage.
    $error = t('You do not have permission to access this page.');
    return clever_render_error(CLEVER_ERROR_NO_SSO_ACCESS, $error);
  }

  // 4. Updates user account data from Clever.
  drupal_alter('clever_user_presave', $account, $account_data, $me['type']);
  user_save($account);

  // 5. Mutates the global user object into the desired user account.
  $user = $account;
  $front = variable_get('site_frontpage', 'node');
  $edit = [
    'redirect' => $front,
    'clever_access_token' => $access_token,
  ];

  $link = l(t('Account'), 'user/' . $account->uid);
  $args = [
    '@name' => $account->name,
    '@data' => variable_get('clever_debug', FALSE) ? print_r($user_data, TRUE) : '',
  ];
  watchdog('clever', '@name successfully logged in from Clever. @data', $args, WATCHDOG_INFO, $link);

  // Finalize the login process.
  user_login_finalize($edit);

  // Allows modules to alter the redirect URL.
  $context = [
    'account' => $account,
    'me' => $me,
    'scopes' => $scopes,
    'state' => $state,
  ];
  drupal_alter('clever_login_redirect', $front, $context);

  // Redirects to front page if user_login_finalize() didn't.
  drupal_goto($front);
}
