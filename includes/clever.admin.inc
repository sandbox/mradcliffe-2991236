<?php

/**
 * @file
 * Clever admin callbacks.
 */

/**
 * Clever admin settings form.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form state array.
 *
 * @return array
 *   The form array.
 */
function clever_settings_form(array $form, array &$form_state) {

  $form['clever_client_id'] = [
    '#type' => 'textfield',
    '#title' => t('OAuth2 Client ID'),
    '#description' => t('Provide your Clever application client ID.'),
    '#default_value' => variable_get('clever_client_id', ''),
    '#required' => TRUE,
  ];

  $form['clever_client_secret'] = [
    '#type' => 'textfield',
    '#title' => t('OAuth2 Client Secret'),
    '#description' => t('Provide your Clever application client secret.'),
    '#default_value' => variable_get('clever_client_secret', ''),
    '#required' => TRUE,
  ];

  // Gets all special user roles.
  $role_options = $query = db_select('role', 'r')
    ->fields('r', array('rid', 'name'))
    ->condition('rid', [DRUPAL_ANONYMOUS_RID, DRUPAL_AUTHENTICATED_RID], 'NOT IN')
    ->orderBy('r.name')
    ->execute()
    ->fetchAllKeyed();
  $role_options[''] = t('None');

  $form['clever_role_student'] = [
    '#type' => 'select',
    '#title' => t('Student role'),
    '#description' => t('Choose the role that best describes the "student" type in Clever.'),
    '#options' => $role_options,
    '#default_value' => variable_get('clever_role_student', ''),
  ];
  $form['clever_role_teacher'] = [
    '#type' => 'select',
    '#title' => t('Teacher role'),
    '#description' => t('Choose the role that best describes the "teacher" type in Clever.'),
    '#options' => $role_options,
    '#default_value' => variable_get('clever_role_teacher', ''),
  ];
  $form['clever_role_schooladmin'] = [
    '#type' => 'select',
    '#title' => t('School Administrator role'),
    '#description' => t('Choose the role that best describes the "school_admin" type in Clever.'),
    '#options' => $role_options,
    '#default_value' => variable_get('clever_role_schooladmin', ''),
  ];
  $form['clever_role_districtadmin'] = [
    '#type' => 'select',
    '#title' => t('District Administrator role'),
    '#description' => t('Choose the role that best describes the "district_admin" type in Clever.'),
    '#options' => $role_options,
    '#default_value' => variable_get('clever_role_districtadmin', ''),
  ];

  // Gets all potential fields.
  module_load_include('inc', 'field', 'field.info');
  $field_map = field_info_field_map();
  $user_fields = ['' => t('None')];
  foreach ($field_map as $field_name => $field_info) {
    if (isset($field_info['bundles']['user']) && $field_info['type'] === 'text') {
      $user_fields[$field_name] = $field_name;
    }
  }

  // Clever v2.0 requires that applications store the clever ID. A module may
  // want to provide some other integration point so this configuration is
  // "optional".
  $form['clever_field_user_id'] = [
    '#type' => 'select',
    '#title' => t('User Clever ID Field'),
    '#description' => t('Choose the user field that should be associated with the Clever ID.'),
    '#options' => $user_fields,
    '#default_value' => variable_get('clever_field_user_id', ''),
    '#required' => FALSE,
  ];

  $form['clever_field_user_firstname'] = [
    '#type' => 'select',
    '#title' => t('User First Name or Full Name Field'),
    '#description' => t('Choose the user field that should be associated with name.first, or if the following fields are empty, the full name of the user will be set.'),
    '#options' => $user_fields,
    '#default_value' => variable_get('clever_field_user_firstname', ''),
    '#required' => FALSE,
  ];

  $form['clever_field_user_middlename'] = [
    '#type' => 'select',
    '#title' => t('User Middle Name Field'),
    '#description' => t('Choose the user field that should be associated with name.middle'),
    '#options' => $user_fields,
    '#default_value' => variable_get('clever_field_user_middlename', ''),
    '#required' => FALSE,
  ];

  $form['clever_field_user_lastname'] = [
    '#type' => 'select',
    '#title' => t('User Last Name Field'),
    '#description' => t('Choose the user field that should be associated with name.last.'),
    '#options' => $user_fields,
    '#default_value' => variable_get('clever_field_user_lastname', ''),
    '#required' => FALSE,
  ];

  $form['clever_debug'] = [
    '#type' => 'checkbox',
    '#title' => t('Log data for debugging'),
    '#description' => t('Logs additional data that may contain personally-identifiable information to the watchdog table. You should not use this in production or empty the watchdog table after you finish using this functionality.'),
    '#default_value' => variable_get('clever_debug', FALSE),
  ];

  // @todo provide mapping fields for clever ID on Organic groups entity fields.

  return system_settings_form($form);
}
