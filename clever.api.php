<?php

/**
 * @file
 * Clever module API documentation.
 *
 * @addtogroup hooks
 * @{
 */

/**
 * Allows modules to alter the found user account (or create a new one).
 *
 * This should never save existing user accounts. Instead you shuold use
 * hook_clever_user_presave_alter() instead. This hook is useful for modules
 * that need to do extra lookup to find an existing user account when it is
 * not found by default or to create new user accounts though modules should
 * check to make sure that "use clever sso" permission is granted so users
 * are not created unnecessarily.
 *
 * @param object $account
 *   The user account loaded by email.
 * @param array $info
 *   An associative array of data from Clever:
 *     - data:
 *       - district: The Clever Ids for the district.
 *       - email: The user's email address.
 *       - name: A name object with first, last and middle keys.
 *       - schools: An array of Clever Ids (possibly empty).
 *       - id: The Clever Id associated with the user.
 *     - links: An indexed array of links with rel and uri keys.
 * @param string $type
 *   The user type e.g. teacher, student.
 */
function hook_clever_user_load_alter($account, array $info, $type) {
  // This example creates a user account automatically.
  if (!$account) {
    $new = drupal_anonymous_user();
    $edit['mail'] = $info['data']['email'];
    $edit['name'] = $info['data']['email'];
    $edit['pass'] = user_password();

    // Lookup role via type parameter.
    $type_name = $type === 'school_admin' || $type === 'district_admin' ? str_replace('_', '', $type) : $type;
    $role = user_role_load_by_name(variable_get('clever_role_' . $type_name, ''));

    if ($role) {
      $rid = $role->rid;
      $new->roles = [
        $rid => $role->name,
      ];
    }

    // It's probably a good idea to notify the user of account creation.
    $account = user_save($new, $edit);
  }
}

/**
 * Allows modules to alter the user account before it is saved.
 *
 * Do any additional mapping or account setup such as integration with
 * profile2 module.
 *
 * @param object $account
 *   The user account to save.
 * @param array $info
 *   An associative array of data from Clever:
 *     - data:
 *       - district: The Clever Ids for the district.
 *       - email: The user's email address.
 *       - name: A name object with first, last and middle keys.
 *       - schools: An array of Clever Ids (possibly empty).
 *       - id: The Clever Id associated with the user.
 *     - links: An indexed array of links with rel and uri keys.
 * @param string $type
 *   The user type e.g. teacher, student.
 */
function hook_clever_user_presave_alter($account, array $info, $type) {
  $account->profile_something->field_something['und'][0]['value'] = $info['data']['name']['first'];
}

/**
 * Allows modules to alter the redirect array.
 *
 * @param string &$path
 *   The front page path by default.
 * @param array $context
 *   An associative array of contextual information with the following keys:
 *     - account: The user account, which is the same as the global user.
 *     - me: The results of the "me" request to Clever.
 *     - scopes: The scopes passed to the application for this user.
 *     - state: This application state passed to Clever and back to this
 *       application, which is most likely an empty string.
 */
function hook_clever_login_redirect_alter(&$path, array $context) {
  $path = 'node/1';
}

/**
 * @} End addtgroup hooks
 */
