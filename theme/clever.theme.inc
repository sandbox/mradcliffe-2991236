<?php

/**
 * @file
 * Clever theme and template preprocess/process functions.
 */

/**
 * Template preprocess clever_error.
 *
 * @param array &$variables
 *   Template variables.
 */
function template_preprocess_clever_error(&$variables) {
  $attributes = [
    'id' => 'clever-error-container',
    'class' => ['messages', 'error', 'clever-error'],
  ];
  if (isset($variables['attributes'])) {
    $attributes = $attributes + $variables['attributes'];
  }

  $variables['content'] = [
    '#type' => 'container',
    '#attributes' => $attributes,
    'message' => [
      '#markup' => $variables['error'],
    ],
    'return' => [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['clever-return-link-wrapper'],
      ],
      'return_link' => [
        '#theme' => 'link',
        '#text' => t('Return to Clever'),
        '#path' => 'https://clever.com/login',
        '#options' => [
          'html' => FALSE,
          'attributes' => [
            'class' => ['clever-return-link'],
          ],
        ],
      ],
    ],
  ];
}
