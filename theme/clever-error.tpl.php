<?php

/**
 * @file
 * Clever Error Template.
 *
 * Variables:
 *   - $error: The error message.
 *   - $title: The title for the error message.
 *   - $content: The content to render.
 *   - $attributes: Attributes from the render array.
 */
?>
<div>
  <?php print drupal_render($content) ?>
</div>
