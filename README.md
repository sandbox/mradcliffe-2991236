# Clever

Provides integration between Drupal and Clever Identity and Data APIs. Clever provides a SaaS middleware between Student Information Systems or similar systems and external applications.

## Supported features

* Clever Identity API Single Sign-On.
   * Supports mapping to an existing user by e-mail address (fallback) or directly with Clever ID, and allows to assign user roles and first/middle/last name to fields.

## API

* `clever_request` allows arbitrary GET/POST requests to Clever Data v2.0 API endpoints as JSON.
* `hook_clever_user_load_alter` allows modification of or creation of new accounts.

## Roadmap

* Instant Login Link embedding support.
* Organic groups support.
* Data API integration.
* Drupal 8:
    * Typed Data API <-> JSON API (json_api module cannot be integrated due to being limited to only entities architecturally rather than all Typed Data).
